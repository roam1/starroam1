<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier
{
	protected $excludedRouteGroups = ['checkout/success', 'checkout/quick-recharge-payment','checkout/failure'];
    public function handle($request, Closure $next)
    {
        if ($this->isReading($request) || ($this->excludedRoutes($request)) || $this->tokensMatch($request)) {
            return $this->addCookieToResponse($request, $next($request));
        }
        Throw new TokenMismatchException;
    }

    protected function excludedRoutes($request)
    {
        foreach($this->excludedRouteGroups as $route) {
            if ($request->segment(1).'/'.$request->segment(2) === $route) {
                return true;
            }
        }

        return false;
    }}
