<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier
{
	/**
 * The URIs that should be excluded from CSRF verification.
 *
 * @var array
 */
 protected $except = [

     'update'
 ]; 
	protected $excludedRouteGroups = ['checkout/success', 'checkout/quick-recharge-payment','checkout/failure'];
	private $openRoutes = ['api-order-status','checkout/success', 'checkout/quick-recharge-payment','checkout/failure'];

    /*public function handle($request, Closure $next)
    {
        if ($this->isReading($request) || ($this->excludedRoutes($request)) || $this->tokensMatch($request)) {
            return $this->addCookieToResponse($request, $next($request));
        }
        Throw new TokenMismatchException;
    }*/
	public function handle($request, Closure $next)
    {
        //add this condition 
		foreach($this->openRoutes as $route) {
	
		  if ($request->is($route)) {
			return $next($request);
		  }
		}
	
		return parent::handle($request, $next);
	  }

    protected function excludedRoutes($request)
    {
        foreach($this->excludedRouteGroups as $route) {
            if ($request->segment(1).'/'.$request->segment(2) === $route) {
                return true;
            }
        }

        return false;
    }}
