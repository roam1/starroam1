<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ERP;
Use App\Models\Plans;
use App\Models\Cartitem;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Orderitem;
use Illuminate\Support\Facades\Input;


class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($CountryId=243)
    {
    	
        $selectedID = $CountryId;
	    $soap_options =  \Config::get('custom.soap_options');
		$countryName="null";
        $connectinType=Plans::StaticConnectionType();
		$TripDuration=Plans::StaticTripDuration();
		$PlanType=Plans::StaticPlanType();
		$TripDuration=['A'];
		$ResultArray=[];
		$AllPlnArray = [];
		$AllArray=[];
		$DurationFilter=[];
		$TypeFilter=[];
		$NetworkFilter=[];
		$countryName='';
		$keey=0;
		$pkey = $plan_key = $pl_key = 0;
	
				
					$AllPlnArray[$plan_key] = $this->getPlanList($connectinType,$TripDuration, $PlanType,$ResultArray,$CountryId,$countryName);
				
		foreach($AllPlnArray as $allpl){
			if(!empty($allpl)){
				foreach($allpl as $allplan){
					$AllArray[$pl_key] = $allplan;
					$pl_key++;
				}
			}
		}
		$ResultArray = $AllArray;
	$AllCounArray[$plan_key] = $this->getCountryList();
	foreach($AllCounArray as $allCn){
			if(!empty($allCn)){
				foreach($allCn as $allcountry){
					$AllCountries[$pl_key] = $allcountry;
					$pl_key++;
				}
			}
		}
			$countries = $AllCountries;
		//dd($countries);
	    return view('frontend.home.index',compact('ResultArray','selectedID','countries'));
	}
	public function getPlans( $CountryId = 243){

	    $selectedID = $CountryId;
	    $soap_options =  \Config::get('custom.soap_options');
		$countryName="null";
		$connectinType=Plans::StaticConnectionType();
		$TripDuration=Plans::StaticTripDuration();
		$PlanType=Plans::StaticPlanType();
		$TripDuration=['A'];
		$ResultArray=[];
		$AllPlnArray = [];
		$AllArray=[];
		$DurationFilter=[];
		$TypeFilter=[];
		$NetworkFilter=[];
		$keey=0;
		$pkey = $plan_key = $pl_key = 0;
		
				
					$AllPlnArray[$plan_key] = $this->getPlanList($connectinType,$TripDuration, $PlanType,$ResultArray,$selectedID,$countryName);
			
		foreach($AllPlnArray as $allpl){
			if(!empty($allpl)){
				foreach($allpl as $allplan){
					$AllArray[$pl_key] = $allplan;
					$pl_key++;
				}
			}
		}
		$AllCounArray[$plan_key] = $this->getCountryList();
	foreach($AllCounArray as $allCn){
			if(!empty($allCn)){
				foreach($allCn as $allcountry){
					$AllCountries[$pl_key] = $allcountry;
					$pl_key++;
				}
			}
		}
		$countries = $AllCountries;
		$ResultArray = $AllArray;
	    $result['home_offer'] = \View::make('frontend.home.home_offer')->with('ResultArray', $ResultArray)->with('selectedID', $selectedID)->with('countries', $countries)->render();
	 
	    return $result;
	
             }
			 
	
	
	
protected function  getPlanList($connectinType,$TripDuration, $PlanType,$ResultArray,$CountryId,$countryName){
	
			$soap_options =  \Config::get('custom.soap_options');
			try{
				$webservice = new \SoapClient('http://www.roam1.com/R1IP/Inquire.asmx?wsdl', $soap_options);
				
			
				$result = $webservice->GetPlans([
						'PartnerId'		=>	'371', 
						'CustomerType'=>'C',
						'ConnectionType'=>'Pre',
						'PlanType'=>'V',
						'CountryOfTravel'=>$CountryId,
						'TripDuration'=>'A',
						'ClientIP'=>\Request::ip(),
						'OfferPlans' => false,
						'OfferName'=>"",
						]);	
						

				
				//dd($result);
			if($result->GetPlansResult->Status==1):
				$ResultPlans=$result->GetPlansResult->ListItem->PlansOfTariff;
				if(isset($ResultPlans) && count($ResultPlans)>0){
					if(count($ResultPlans)>1){
						 foreach($ResultPlans as $eachPlan):
						     $dataInsert=json_decode(json_encode($eachPlan), true);
						   /* insert  PriceforSort in system */
						   if($dataInsert['Price']>$dataInsert['SellingPrice']){
								 $dataInsert['PriceforSort']=$dataInsert['SellingPrice'];
							}else{
							$dataInsert['PriceforSort']=$dataInsert['Price'];
							}
							/* insert  PriceforSort in system */
							$dataInsert['CountryId'] = $CountryId;
							$dataInsert['CountryName'] = $countryName;	
						  $ResultArray[]=$dataInsert;
						 endforeach;
					 }else{
						  $dataInsert=json_decode(json_encode($ResultPlans), true);
						   if($dataInsert['Price']>$dataInsert['SellingPrice']){
						   }else{
							   $dataInsert['PriceforSort']=$dataInsert['Price'];
						   }
						   $dataInsert['CountryId'] = $CountryId;
						   $dataInsert['CountryName'] = $countryName;	
						  $ResultArray[]=$dataInsert;
					 }
				}
			endif;	
			}catch(Exception $e){

			}

	return $ResultArray;
	}
	protected function  getCountryList(){
	
			
			$soap_options =  \Config::get('custom.soap_options');
			
			
			try{
				$webservice = new \SoapClient('http://www.roam1.com/R1IP/Inquire.asmx?wsdl', $soap_options);
				
			
				$result = $webservice->GetCountries([
						'PartnerId'		=>	'371', 
					    'ClientIP'=>\Request::ip()
						
						]);	


				
				//dd($result);
			if($result->GetCountriesResult->Status==1):

				$ResultPlans=$result->GetCountriesResult->ListItem->Countries;

				if(isset($ResultPlans) && count($ResultPlans)>0){
					if(count($ResultPlans)>1){
						 foreach($ResultPlans as $eachPlan):
						     $dataInsert=json_decode(json_encode($eachPlan), true);
						   
                        
						
						  $ResultCountry[]=$dataInsert;
						//print_r($ResultCountry);
						 endforeach;
					 }else{
						  $dataInsert=json_decode(json_encode($ResultPlans), true);
						  
						  
						  $ResultCountry[]=$dataInsert;
					 }
				}
			endif;	
			}catch(Exception $e){

			}

	return $ResultCountry;
	}
	public function bubbleSort(array $array) {
		$array_size = count($array);
		for($i = 0; $i < $array_size; $i ++) {
			for($j = 0; $j < $array_size; $j ++) {
				if ($array[$i] < $array[$j]) {
					$tem = $array[$i];
					$array[$i] = $array[$j];
					$array[$j] = $tem;
				}
			}
		}
		return $array;
	}
	
 
	public function postAddToCart(Request $request){
		
		//dd($request->get('Validity'));

		//dd($request);
		$CountryIds=explode(',',$request->get('CountryId'));
		if($request->get('PlanId') && $request->get('PlanName') && !empty($CountryIds[0]) ) {
			
			$cart = new Cart;
			$addtoCart = $cart->AddToCart($request->get('PlanId'),$request->get('PlanName'),$CountryIds[0],
					$request->get('PlanDescription'),$request->get('Price'),$request->get('Currency'),$request->get('Validity'),
					$request->get('planjson'),$request->get('CountryId'),$request->get('TravFrom'),$request->get('TravTo'));
			if($addtoCart){

           $cartObject = new Cart;
           $allcontents = $cartObject->getItems();
			
				/* =========================== PageType record store into session for GA Code ================================== */
				if (\Session::has('PageType')) \Session::forget('PageType');
					\Session::put('PageType',$request->get('PageType'));
				/* =========================== order record store into session for GA Code ================================== */
			
				\Session::flash('message','Plan has been successfully added.');
				\Session::flash('alert-class', 'alert-success');
				
				return redirect('cart');
			}	
		}
		\Session::flash('message','Failed to  Cart plan.');
		\Session::flash('alert-class', 'alert-error');	
		return redirect()->back();
	}
	
	
}
