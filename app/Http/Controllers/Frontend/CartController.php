<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cartitem;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Orderitem;
use App\Models\State;
use App\User;
use App\Models\SiteSetting;
use App\Models\ERP;
use App\Models\Plans;
use App\Models\Customer;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests\Frontend\Auth\LoginRequest; 
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\City;
use App\Models\Country;
use DB;
class CartController extends Controller
{
	
	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * User model instance
     * @var User
     */
    protected $user; 
    
    /**
     * For Guard
     *
     * @var Authenticator
     */
    protected $auth;
	/**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth, User $user)
    {
        $this->user = $user; 
        $this->auth = $auth;
        //$this->middleware('guest', ['except' => 'getLogout']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(){
     	
        $percheck = '';
		$deliverytype = '';
	
		$cartObject = new Cart;
		$cart = $cartObject->getItems();
		$subTotal = $cartObject->subTotal();
	//	dd($subTotal);
	
		$currency = SiteSetting::where(array('attribute_code'=>'currency'))->first()->attribute_value;
		//$deliveryCharge = SiteSetting::where(array('attribute_code'=>'delivery_charge'))->first()->attribute_value;
		$taxPercentage = SiteSetting::where(array('attribute_code'=>'tax'))->first()->attribute_value;
		
	
		if(\Session::get('Checkoutpincode') != '')
		{
			$pinCode = \Session::get('Checkoutpincode');
		}
		else
		{
			$pinCode = (isset($cart->address_pincode)?$cart->address_pincode:'');
		}

		$plande = new Plans;	
	    $result=$plande->apiPin($pinCode);
//dd($result);
	$result=json_decode(json_encode($result), true);
	$deliveryCharge = '';
 if(isset($result['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge']))
		{
			if($result['CheckExpressDeliveryResult']['Status'] == "1" && $result['CheckExpressDeliveryResult']['ListItem']['Cities']['IsExpress'] != "1"){

				$deliveryCharge = $result['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge'];
			}
			else
			{
			$deliveryCharge = $result['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge'];
				}
		}
		if(isset($cart->id) && $cart->id>0) {
			//$promition->applycoupon($cart->id, $subTotal, $currency, $deliveryCharge, $taxPercentage);
		}
		$cart = $cartObject->getItems();
		//dd($cart);
		/*for addons */
		$ResultAddon=[];
		$result= [];
		$i =0;
		$AddonArr = array();
		$taxxlabel = '';
		$taxPercentage='';
		/* konnect int */
		$plankonnect = "";
			$total_cartitem = '';
			$nplankonnect = '';
		if(isset($cart->items)){
			/* konnect int */
			

			/* konnect int */
			$total_cartitem = count($cart->items);

			foreach ($cart->items as $cartItem)
			{
				$planjsonDecode=json_decode($cartItem->planjson);
				
				if($planjsonDecode->Validity == "1")
				{
					$percheck = 'has';
					if(!isset($planjsonDecode->PerDayPlanDurationCount)){
					$planjsonDecode->PerDayPlanDurationCount = "1";
					$cartItem->planjson = json_encode($planjsonDecode);
					
					$cartItem->save();
					}


				}
				
					if(isset($planjsonDecode->deliverytype) && $planjsonDecode->deliverytype == 'regular' )
						{
							$deliverytype = "regular";	
						}

				if(isset($planjsonDecode->deliverytype) && $planjsonDecode->deliverytype == 'express' )
						{
							$deliverytype = "express";	
						}
	
				
				$promition->applycoupon($cart->id, $subTotal, $currency, $deliveryCharge, $taxPercentage);
				if($planjsonDecode->PlanType == "K")
				{
					$plankonnect = "has";
				}

				if($planjsonDecode->PlanType != "K")
				{
					$nplankonnect = "nhas";
				}




			}

			if($plankonnect == "has")
			{
				foreach ($cart->items as $cartItem)
				{
					$planjsonDecode=json_decode($cartItem->planjson);
					if($planjsonDecode->PlanType == "K")
					{
							$this->removeItemByItemId($cartItem->id);
							if($cartItem->Quantity<1){
							return redirect()->back();
						
							}
					}
				}

			}
		
			/* konnect int */


			foreach ($cart->items as $planId){
	
					$soap = ERP::SoapInfo();
					$soap_options =  \Config::get('custom.soap_options');
					$webservice = new \SoapClient($soap->inquiry_url, $soap_options);
					try {
						// Calle API & fetch user logged information
						$soapResult = $webservice->GetAddons([
							'PartnerId'		=>	$soap->partner_id, 
							'PlanId'		   =>	$planId->PlanId,
							'ClientIP'		 =>    \Request::getClientIp(true)
						]);
						$response = json_decode(json_encode($soapResult->GetAddonsResult->Status), true);
						if ($response) {
							
							$ListItem = json_decode(json_encode($soapResult->GetAddonsResult->ListItem), true);
							
							if(!empty($ListItem)){
							
							$addons=json_decode(json_encode($soapResult->GetAddonsResult->ListItem->Addon), true);
							
							if(is_array($addons) && !empty($addons)):
								foreach($addons as $item){
									
									$i++;
								}
							endif;
							}
						}
						//echo '<pre>';
						//var_dump($result);
						
						$AddonArr = $ResultAddon + $result;
						//dd($AddonArr);
					} catch(SOAPFault $ex ){
						return $this->errors = $ex->getMessage();
					}
				
			}
		
			if (\Session::has('CurrentAddOnlist')) {
				\Session::forget('CurrentAddOnlist');
			}
			\Session::put('CurrentAddOnlist',  $AddonArr);

	
		/* here to check customer loged in give tax accord */
		if(!Auth::user()){
		if($taxPercentage > 0)
		{
			$cartObject = new Cart;
		$cart = $cartObject->getItems();
			$taxxlabel = 'IGST '.number_format($cart->tax_percnt, 0).'%';
		}

		}else{
			/* logged in customer */			
			$customerId = User::getCustomerIdByUserId(\Auth::id());
			
			$customer = DB::table('customer_address')->where('customer_id', $customerId)->get();
			
			if($taxPercentage > 0)
				{	
				 if(strpos($customer[0]->StateName, 'Delhi') !== FALSE)
					{
						$taxxlabel  = "(CGST : 9% and SGST : 9%)";
					}
					elseif(strpos($customer[0]->CityName, 'Delhi') !== FALSE)
					{
						$taxxlabel  = "(CGST : 9% and SGST : 9%)";
						//$taxxlabel  = number_format($cartItems->tax_percnt, 0)."%";
					}
					else
					{
						$taxxlabel  = "IGST ".number_format($taxPercentage, 0)."%";
					}
		 		}		
			}

		}
	
		
		$cart = $cartObject->getItems();

/* konnect int */
				
		return view('frontend.cart.index')
			->with('cartItems', $cart)
			->with('cartMaster', $cart)	
			->with('taxxlabel', $taxxlabel)			
			->with('countries',Country::AllPlanCountries())
			->with('addons', $AddonArr)
			->with('percheck', $percheck)
			->with('total_cartitem',$total_cartitem)
			->with('plankonnect',$plankonnect)
			->with('percheck', $percheck)
			->with('deliverytype', $deliverytype)
			->with('nplankonnect',$nplankonnect);
    }
	
	

	
	public function getStep2(){
		
		$percheck = '';
		$deliverytype = '';
		$deliveryCharge = "0";
		$optexp = '';
		$allcities = City::WHERE ('StateId', '!=', 44)->orderBy('CityName', 'ASC')->lists('CityName','CityId');
		
		$cartObject = new Cart;
		$cartItems = $cartObject->getItems();
		//dd($cartItem);


		 if(count($cartItems)<1)
		 {
			 
		  
			return view('frontend.cart.step2')->with('cartItems', $cartItems);   
		  } 
		

		if(isset($cartItems->address_email) && $cartItems->address_email != ''){
			$cartAddr = Cart::WHERE ('address_email', '=', $cartItems->address_email)->orderBy('id','DESC')->first();
		}
		/* Check Custmer is logged then Prefill ddata */
		if (!empty(\Auth::user())) {
		
		$user = User::with('customer')->find(Auth::user()->id);
	
		if(!is_null($user->customer)){
		$customer_address = Customer::with('addresses')->find($user->customer->CustomerId);
		foreach($customer_address->addresses as $singale_address){
			if($singale_address->address_type == 'B'){
				$address = $singale_address;
				break;
			}
		}
			$taxxlabel = '';
		if(empty($cartItems->address_name)){
			$cartItems->address_name = $user->FirstName.' '.$user->LastName;
		}
		if(empty($cartItems->address_email)){
			$cartItems->address_email = $user->email;
		}
		if(!empty($address)){
			if(empty($cartItems->address_address)){
			$cartItems->address_address = $address->AddressLine1;
			}
			if(empty($cartItems->address_landmark)){
				$cartItems->address_landmark = $address->AddressLine2;
			}
			if(empty($cartItems->address_state)){
				$cartItems->address_state = $address->StateName;

				if(strpos($cartItems->address_state, 'Delhi') !== FALSE)
				{
					$taxxlabel  = "(CGST : 9% and SGST : 9%)";
				}
				
			}
			

			if(empty($cartItems->address_city)){
				$cartItems->address_city = $address->CityName;
			}
			if(empty($cartItems->address_pincode)){
				$cartItems->address_pincode = $address->PinCode;
			}
			if(empty($cartItems->address_fromdate)){
				$cartItems->address_fromdate = $address->address_fromdate;
			}
			if(empty($cartItems->address_todate)){
				$cartItems->address_todate = $address->address_todate;
			}
		}
		if(!empty($cartAddr)){
			if(!empty($cartItems->address_address)){
			$cartItems->address_address = $cartAddr->address_address;
			}
			if(!empty($cartItems->address_landmark)){
				$cartItems->address_landmark = $cartAddr->address_landmark;
			}
			if(!empty($cartItems->address_state)){
				$cartItems->address_state = $cartAddr->address_state;
				
				
			}
			
			if(!empty($cartItems->address_city)){
				$cartItems->address_city = $cartAddr->address_city;
				if(strpos($cartAddr->address_state, 'Delhi') !== FALSE)
				{
					$taxxlabel  = "(CGST : 9% and SGST : 9%)";
				}
				else
				{
					if(isset($cartItems->tax_percnt))
						{
					$taxxlabel  = "IGST ".number_format($cartItems->tax_percnt, 0)."%";
						}
				}
				
			}
			if(!empty($cartItems->address_pincode)){
				$cartItems->address_pincode = $cartAddr->address_pincode;
			}
			if(!empty($cartItems->address_fromdate)){
				$cartItems->address_fromdate = $cartAddr->address_fromdate;
			}
			if(!empty($cartItems->address_todate)){
				$cartItems->address_todate = $cartAddr->address_todate;
			}	
		}
		
		if(empty($cartItems->address_mobile)){
			$cartItems->address_mobile = $user->customer->mobile;
		}
	 }
	 
	}
	else{
		$taxxlabel = '';
		if(isset($cartItems->tax_percnt))
		{
		$taxxlabel = 'IGST '.number_format($cartItems->tax_percnt, 0).'%';
		}
	}
		$states = State::AllStates()->toArray();
		
		if(!empty($cartItems->address_state) && (array_search($cartItems->address_state,$states)!= false)){
			$Cites = new City;
			$cities = $Cites->CityById(array_search($cartItems->address_state,$states));
		}else{
			$cities =[];		
		}
		/* konnect int */
		$plankonnect = "";
		$total_cartitem = "";
		$nplankonnect = '';
		if(isset($cartItems->items)){
			$total_cartitem = count($cartItems->items);
			
			
			
			if(\Session::get('Checkoutpincode') != '')
		{
			$pinCode = \Session::get('Checkoutpincode');
		}
		else
		{
			$pinCode = $cartItems->address_pincode;
		}

		
		

		
			if(isset($cartItems->id) && $cartItems->id>0) {
			$currency = SiteSetting::where(array('attribute_code'=>'currency'))->first()->attribute_value;
			$taxPercentage = SiteSetting::where(array('attribute_code'=>'tax'))->first()->attribute_value;
			$subTotal = $cartObject->subTotal();
		//	$promition = new Promotions;



					$cartMaster = Cart::find($cartItems->id);
					$deliveryCharge = '';
					if(isset($result['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge']))
		{
			if($result['CheckExpressDeliveryResult']['Status'] == "1"  && $result['CheckExpressDeliveryResult']['ListItem']['Cities']['IsExpress'] != "1"){

				$deliveryCharge = $result['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge'];
			}
			else
			{
			$deliveryCharge = $result['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge'];
				}

		}
		
			
		
		}
		
			
			foreach ($cartItems->items as $cartItem)
			{
				$planjsonDecode=json_decode($cartItem->planjson);

				$planjsonDecode=json_decode($cartItem->planjson);
				$optexp = (isset($planjsonDecode->deliverytype)?$planjsonDecode->deliverytype:'');
				
				if($planjsonDecode->PlanType == "K")
				{
					$plankonnect = "has";
				}
				if(isset($planjsonDecode->deliverytype) && $planjsonDecode->deliverytype == 'regular' )
				{
					$deliverytype = "regular";	
				}

				if(isset($planjsonDecode->deliverytype) && $planjsonDecode->deliverytype == 'express' )
				{
					$deliverytype = "express";	
				}
					if($planjsonDecode->Validity == "1")
				{
					$percheck = 'has';
				}
				
				if($deliveryCharge== "0" && $percheck=="")
				{
					$deliverytype = "regular";	
				}
				
				if($planjsonDecode->PlanType != "K")
				{
					$nplankonnect = "nhas";
				}
			}
		}
		
		$cartObject = new Cart;

		$cartItems = $cartObject->getItems();
		$deliveryCharge =  $cartItems->delivery_charge;

			if(isset($cartItems->id) && $cartItems->id>0 && $percheck=="" && $deliveryCharge<="0") {

		$cartMaster = Cart::find($cartItems->id);
		$deliverytype = "regular";
		if(\Session::get('Checkoutpincode') != '')
		{
			$pinCode = \Session::get('Checkoutpincode');
		}
		else
		{
			
				
			$pinCode = $cartItems->address_pincode;
		}
		$deliv = \Request::input('deliverytype');

		if($optexp == "express"){

		\Session::flash('message','Sorry Pincode '.$pinCode.' not apply for Express Delivery.');
		}

			}
			
		/* add for getting Airport details */
		//$AirportList=Airport::getAirportList();
		//$AirportAddress=Airport::getAirportAddress();	
		$fromdate = \Session::get('TripFromDate');
		$todate = \Session::get('sushovan');
		return view('frontend.cart.step2')
			->with('cartItems', $cartItems)
			->with('cartMaster', $cartItems)
			//->with('AirportList',$AirportList)
			//->with('AirportAddress',$AirportAddress)
			->with('states', $states)
			->with('taxxlabel',$taxxlabel)
			->with('countries',Country::AllPlanCountries())
			->with('fromdate',$fromdate)
			->with('todate',$todate)
			->with('allcities',$allcities)
			->with('percheck', $percheck)
			->with('deliverytype', $deliverytype)
			//->with('mobidis',$mobidis)
			->with('cities',$cities)
			->with('plankonnect', $plankonnect)
			->with('nplankonnect', $nplankonnect)
			->with('total_cartitem', $total_cartitem);
	}


	
	public function postStep2(Request $request) {
		
		/* Save  address data at Cart object */
		//$request->all();
		$cartObject = new Cart;
		$c = $cartObject->getItems();
		
		$deliverytype = '';
		$deliveryCharge = '';
		$perday = '';
		$pincode = \Request::input('address_pincode');
		if($pincode=='')
		{
		$pincode = $c->address_pincode;	
		}
		$plande = new Plans;	
		$isExpress=$plande->apiPin($pincode);
		
		$isExpress=json_decode(json_encode($isExpress), true);
		if(isset($isExpress['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge']))
		{
			//$promition = new Promotions;
			if($isExpress['CheckExpressDeliveryResult']['Status'] == "1" && $isExpress['CheckExpressDeliveryResult']['ListItem']['Cities']['IsExpress'] != "1"){

				$deliveryCharge = $isExpress['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge'];
			}
			else
			{
			$deliveryCharge = $isExpress['CheckExpressDeliveryResult']['ListItem']['Cities']['ExpressCharge'];
				}
				$taxPercentage = SiteSetting::where(array('attribute_code'=>'tax'))->first()->attribute_value;
			$promition->applycoupon($c->id, $c->sub_total, $c->currency, $deliveryCharge, $taxPercentage, $pincode);
		}
		$cartObject = new Cart;
		$c = $cartObject->getItems();
		$cartObject = Cart::find($c->id);
		$cartObject->fill($request->all()); 
		$taxPercentage = SiteSetting::where(array('attribute_code'=>'tax'))->first()->attribute_value;
		
		$currency = SiteSetting::where(array('attribute_code'=>'currency'))->first()->attribute_value;
		
		
		
		$cartObject=Cart::setDocnameAtCart($cartObject,$request);
		
		/*
		 * there are some issue at Data in Cart object 
		 * then redirect to Step2 page
		 */
		 
		if(!$cartObject){
			// redirect to step2 page
			\Session::flash('message', 'Order Failed');
			\Session::flash('alert-class', 'alert-error');
			return redirect()->route('step-2'); 			
		}
		
		/* save Cart object Object */
		
		try{
			$cartObject->save();
		}catch(Exception $e){
			// Some errors redirect to redirect to step2 page
			\Session::flash('message', 'Order Failed');
			\Session::flash('alert-class', 'alert-error');
			return redirect()->route('step-2'); 
		}
		
		$cartMaster = $cartObject->getItems();
		
		if(is_null($cartMaster)):
			// Current does have any Cart item
			// Redirect to page
			\Session::flash('message', 'Order Failed');
			\Session::flash('alert-class', 'alert-error');
			return redirect()->route('step-2'); 
		
		endif;
		$requestDataSession=[];
		
		if (\Session::has('cartMaster')) \Session::forget('cartMaster');
			\Session::put('cartMaster',$cartMaster);
			
		/*
		 * COnvert Cart object to Order object 
		 */
		  
			$requestDataSession=[];
			$orderMaster = new Order;			
			$orderMaster = Order::CartToOrder($orderMaster,$cartMaster);	
			if(!$orderMaster):
				// There are some issue at Data assing from Cart to Order 
				\Session::flash('message', 'Order Failed');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('step-2'); 
			endif;
			
			/* save Order Object */
			
			try{ 
				$orderMaster->save();
				
			}catch (Illuminate\Database\QueryException $e){	
				\Session::flash('message', 'Order Failed');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('step-2'); 
				// Some errors redirect to redirect to step2 page
			}catch(Exception $e){
				\Session::flash('message', 'Order Failed');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('step-2'); 				
				// Some errors redirect to redirect to step2 page
			}
			$requestDataSession['name']=$orderMaster->address_name;
			$requestDataSession['email']=$orderMaster->address_email;
			$requestDataSession['mobile']=$orderMaster->address_mobile;
			
			/* =========================== order record store into session for GA Code ================================== */
			if (\Session::has('orderMaster')) \Session::forget('orderMaster');
				\Session::put('orderMaster',$orderMaster);
			/* =========================== order record store into session for GA Code ================================== */
			
			/* Convert Cart item to Order  Item object */	
			$orderItems = [];
			$cartItems = Cartitem::where(array('cart_id'=>$cartMaster->id))->get();
				
			if($cartItems->isEmpty()):
			
				// redirect to Cart page
				\Session::flash('message', 'There is no plan in your cart');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('home'); 				
			endif;	
			
			/* Start to Convert Cart item to Order item */
			 
			foreach($cartItems as $cartItem){
				//$orderItems[] = array('PlanId'=>$cartItem->PlanId,'ICCID'=>'');
				$orderItem = new Orderitem;
				$orderItem->order_id = $orderMaster->id;
				/* konnect */
				$orderItem->planjson = $cartItem->planjson;
				$orderItem=Cart::assignDataFromCartItemtoOrderItem( $orderItem, $cartItem);
				
					if(!$orderItem):
					// There are some issue at Data assing  Cart Item data from Order Item
					\Session::flash('message', 'Order Failed');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2'); 
					endif;	
					/* Save  Cart Item data from Order Item */
					
					try{	
							$orderItem->save();
					}catch (Illuminate\Database\QueryException $e){	
						\Session::flash('message', 'Order Failed');
						\Session::flash('alert-class', 'alert-error');
						return redirect()->route('step-2'); 
						// Some errors redirect to redirect to step2 page
					}catch(Exception $e){

						\Session::flash('message', 'Order Failed');
						\Session::flash('alert-class', 'alert-error');
						return redirect()->route('step-2'); 
						
						// Some errors redirect to redirect to step2 page
					}
			}
			// for each order quatity to api loop [29.06.16]
				foreach($cartItems as $cartItem){
					$quantity = 0;
					$quantity = $cartItem->Quantity;
					/* coupon code start */
					$planjsonDecode=json_decode($cartItem->planjson);
					$couponcode = '';
						if(isset($planjsonDecode->coupon_code))
							{
							$couponcode = $planjsonDecode->coupon_code;
							}	

							$pdays = '';
							if(isset($planjsonDecode->PerDayPlanDurationCount))
							{
							$pdays = $planjsonDecode->PerDayPlanDurationCount;
							}	


					for($i=0; $i<$quantity; $i++){
						
						$orderItems[] = array('PlanId'=>$cartItem->PlanId,'ICCID'=>'', 'CouponCode'=>$couponcode, 'PerDayPlanDurationCount'=>$pdays);
					}
					/* end */
				}
				
			// for each order quatity to api loop [29.06.16]

				$orderDetails = Order::where(array('id'=>$orderMaster->id))->first();
				
				$date = strtotime("+2 day");
				$deliveryDate = date('Y-m-d', $date);
				/* =========================== order record store into session for GA Code ================================== */
				$orderedItems = Orderitem::where(array('order_id'=>$orderMaster->id))->get();
				if (\Session::has('orderedItems')) \Session::forget('orderedItems');
					\Session::put('orderedItems',$orderedItems);
				/* =========================== order record store into session for GA Code ================================== */
							
		
								
			$CustomerId=false; 
			 
			$soap = ERP::SoapInfo();
			$soap_options =  \Config::get('custom.soap_options');
			$soapClient = new \SoapClient($soap->publish_url, $soap_options);		 
		 if(empty(\Auth::user())){	
			/* for old customer */
			$CustomerId = User::getCustomerIdByEmail($request->address_email);

			 $customer = new Customer;
			 if($CustomerId != NULL){
			$csad=$this->makeOrderToCustomeraParameter($orderDetails); 	
			$customer = new Customer;
			//$result=$customer->UpdCustomerV1($csad['CustomerData'],$csad['CustomerAddresses'], $CustomerId);
				}
				
			/*if(isset($CustomerId) && $CustomerId == NULL){
				\Session::flash('alert-class', 'alert-error');
				\Session::flash('loginMsg', "Order Failed, Please try again.(cd)");
				return redirect()->route('step-2');	
			} */
			
		 }
		 if($CustomerId == NULL){
		 if (!empty(\Auth::user())){
				Auth::user()->id;
				$CustomerId = User::getCustomerIdByUserId(Auth::user()->id);
				 if($CustomerId != NULL){
					$csad=$this->makeOrderToCustomeraParameter($orderDetails); 	
					$customer = new Customer;
					//$result=$customer->UpdCustomerV1($csad['CustomerData'],$csad['CustomerAddresses'], $CustomerId);
				}
			// recreate Order address  for Customer 
		  }else{
			  /* Section for not loggedin Customer */
			 
			  $data=$this->makeOrderToCustomeraParameter($orderDetails);
			  
			   /* Create New  Customer */
			   $customer = new Customer;
			   
			   $result=$customer->RegisterCustomerV1($data['CustomerData'],$data['CustomerAddresses']);
			   
			   /* If get any error during Checkout then
			    *  goto step2 page
				*/
				
			   if(!$result->api_status){
				   
				   /* if Customer is already register  at APi Then thrown error*/
				   if($result->api_Message == "The customer is already registered with Roam1."){
					 // redirect to step when thrown  error 

						\Session::flash('message', 'The email id is already registered, please login
						to continue');
						\Session::flash('loginOpen', "yes");
						\Session::flash('loginMsg', "Please Login");
						\Session::flash('alert-class', 'alert-error');
						return redirect()->route('step-2'); 

					   
				   }elseif($result->api_Message == "The email has already been taken."){
					 // redirect to step when thrown  error 

						\Session::flash('message', 'The email id is already registered, please login
						to continue');
						\Session::flash('loginOpen', "yes");
						\Session::flash('loginMsg', "Please Login");
						\Session::flash('alert-class', 'alert-error');
						return redirect()->route('step-2'); 

					   
				   }else{
					//  d  sushovan **************************************
						\Session::flash('message', 'The email id is already registered, please login
						to continue');
						
						\Session::flash('alert-class', 'alert-error');
						\Session::flash('loginOpen', "yes");
						\Session::flash('loginMsg', "Please Login");
						return redirect()->route('step-2');
				   }
				}else{
					// true and for the customer id of newly register customer
						$CustomerId = $result->api_CustomerId;
				}
			  
		  }
		}
	
		/* Update customer address [01.07.2016] */
		
		if($request->hasFile('passport_front') && $request->hasFile('passport_back')){
		  /* Move Document to CRM Folder */
		  $MoveDocsResult = $this->moveDocumentTOCrm($request,$CustomerId,$orderDetails);
		  /***************** set to null to test sushovan ****************************************** */
		  if(!$MoveDocsResult):
			/* Upload Document Issue  and redirect to Step2 pages */

					\Session::flash('message', 'Order has been failed,please try again.');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2');			
		  endif;
		  
		  if(!empty($MoveDocsResult)){
			//dd($MoveDocsResult);	
				$uploadData = [
								'PartnerId'=>$soap->partner_id,
								'SubscriberId'=>$CustomerId,
								'SimNo'=>'',
								'PassportNo'=>'O55',
								'PassportExpiry'=>date("2017-01-01"),
								'VisaExpiry'=>date("2017-01-01"),
								'CityId'=>'24',
								'Passportfront'=>$MoveDocsResult['pass_front_name'] ,
								'PassportBack'=>$MoveDocsResult['pass_back_name'],
								'Visa'=>$MoveDocsResult['visa'],
								'ClientIP'=>\Request::getClientIp(true)
				];
				
			$upload_status = Customer::PostUploadDocuments( $uploadData );	
			
			if(!$upload_status){
				// There are an issue with API Method  : UploadDocuments
				// redirect to  Stepn 2 pages

					\Session::flash('message', 'Please upload documents.');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2'); 
			}	
		}
		}
		/* Push Customer traval dates at api*/
		
			
			$Status=Customer::PostCustomerTravelPlan($CustomerId,
						date('Y-m-d',strtotime($orderDetails->address_fromdate)),
						date('Y-m-d',strtotime($orderDetails->address_todate)),
						$cartMaster->trav_country
					);
			
			if(!$Status):
				\Session::flash('message', 'Order Failed due to Travel date.');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('step-2'); 
			endif;	
			
		
		/* end Customer traval data at api*/  
		
		/* Create an order at APi */

		$OrderId = false;
		$cartObject = new Cart;
		$c = $cartObject->getItems();
		$pincode = $c->address_pincode;
		$plande = new Plans;	
		
		$isExpress=$plande->apiPin($pincode);
		$isExpress=json_decode(json_encode($isExpress), true);

	
		$ps = '';
	//	print_r($isExpress);

		if(isset($isExpress['CheckExpressDeliveryResult']['Status']) && $isExpress['CheckExpressDeliveryResult']['Status'] == "1")
		{

	if(isset($isExpress['CheckExpressDeliveryResult']['ListItem']['Cities']['IsExpress']) && $isExpress['CheckExpressDeliveryResult']['ListItem']['Cities']['IsExpress'] == "1")
				{
					$ps = true; 
				}
				else
				{
					$ps = false; 
				}


		}
		else
		{
			$ps = false; 
		}	
		
		if($perday == "" && ($deliverytype == "regular" || $deliverytype == ""))
		{
			$ps = false;
		}	
		
		 try{
			 $soapResult = $soapClient->CreateOrder([
								'PartnerId'=>$soap->partner_id,
								'CustomerId'=>$CustomerId,
								'PaymentType'=>'CC',
								'DeliveryDate'=>$deliveryDate,
								'oOrderList'=>$orderItems,
								'ClientIP'=>\Request::getClientIp(true),
								'IsExpress'=>$ps
								]);
								
				//echo $CustomerId."<br>";echo $deliveryDate."<br>";print_r ($orderItems); echo "<br>";/*echo $CountryOfTravel."<br>";*/ exit;
			
			/* mail for order track for order api [04.07.2016]*/
			$customer_email='kanobolbona@gmail.com';
			$site_email='kanobolbona@gmail.com';
			$site_name="Track";
			$DataTrack1=[
							'PartnerId'=>$soap->partner_id,
							'CustomerId'=>$CustomerId,
							'PaymentType'=>'CC',
							'DeliveryDate'=>$deliveryDate,
							'oOrderList'=>$orderItems,
							'ClientIP'=>\Request::getClientIp(true)
						];
		
								
			if (!json_decode(json_encode($soapResult->CreateOrderResult->Status), true)) {
					// There are an issue at Soap API result 	
					// redirect to Checkouy step2
					\Session::flash('message', 'Order Failed due to Travel date.');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2'); 
					
			}
			
			if (json_decode(json_encode($soapResult->CreateOrderResult->Status), true) && 
				json_decode(json_encode($soapResult->CreateOrderResult->OrderId), true) == 0) {

						\Session::flash('message', 'Order failed');
						\Session::flash('alert-class', 'alert-error');
						return redirect()->route('step-2'); 
					
						// Invalid Order generated goto Checkout
			}	
			$OrderId = json_decode(json_encode($soapResult->CreateOrderResult->OrderId), true);				
			}catch(SoapFault $SoapEx){
				// Issue at Order creation at  API;
				// redirect to Checkouy home
					\Session::flash('message', 'Order failed');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2'); 				
			 }
			catch(Exception $e){
				// redirect to Checkouy step2
					// Issue at Order creation at  API;
				\Session::flash('message', 'Order failed');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('step-2'); 
			}
			
			if(!$OrderId):
				// Invalided order id
				\Session::flash('message', 'Order failed');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('step-2'); 
				
			endif;
			
			$orderMaster->soap_order_id = $OrderId;
			
			/* document name update */
			 if(!empty($MoveDocsResult)){
					if(isset($MoveDocsResult['pass_front_name']) && $MoveDocsResult['pass_front_name'] != '' && 
							isset($MoveDocsResult['pass_back_name']) && $MoveDocsResult['pass_back_name'] != ''
					){
					$orderMaster->passport_front = $MoveDocsResult['pass_front_name'];
					$orderMaster->passport_back = $MoveDocsResult['pass_back_name'];
					if(isset($MoveDocsResult['visa_front_name'])){
						$orderMaster->visa_front = $MoveDocsResult['visa_front_name'];
					}
					if(isset($ticket_front_name)){
						$orderMaster->ticket = $MoveDocsResult['ticket_front_name'];
					}
					}
				}
				/* document name update */			
				try{ 
					$orderMaster->save();
					
				}catch (Illuminate\Database\QueryException $e){	
					\Session::flash('message', 'Order has been failed,please try again');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2');					
					// Some errors redirect to redirect to step2 page
				}catch(Exception $e){
					\Session::flash('message', 'Order has been failed,please try again');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2');					
					// Some errors redirect to redirect to step2 page
				}
			/* Genearate Payment reference no 
			 */
			$PaymentReferenceNo= Cart::GetPaymentReferenceNo([
									'PaymentMode' => 'CC',
									'Amount' => $cartMaster->grand_total,
									//'Amount' => 1,
									'PaymentType' => 'O',
									'ReferenceNo' =>$OrderId ,
									'ClientIP' => \Request::getClientIp(true)
					]);
			//$PaymentReferenceNo = 94838;
			if(!$PaymentReferenceNo || ($PaymentReferenceNo == 0)):
				// Error in  PaymentReferenceNo creation 
		
					\Session::flash('message', 'Order has been failed,please try again');
					\Session::flash('alert-class', 'alert-error');
					return redirect()->route('step-2');				
			endif;		

		$orderMastSoapUpdate = Order::find($orderMaster->id);
		if (\Session::has('PayReferenceNo')) \Session::forget('PayReferenceNo');
			\Session::put('PayReferenceNo',$PaymentReferenceNo);
			
		$TransactionId='O'.$PaymentReferenceNo;			 	

		$ccavenueMerchantid=SiteSetting::where('attribute_code', 'like', 'ccavenue_merchant_id')->first()->attribute_value;
		$ccavenueUrl=SiteSetting::where('attribute_code', 'like', 'ccavenue_merchant_url')->first()->attribute_value;
		if (\Session::has('rechargeOp')) \Session::forget('rechargeOp');
		$requestDataSession['PaymentType']='O';
		$requestDataSession['RelationshipNo']=$OrderId;
		$requestDataSession['SystemOrderId']=$orderMastSoapUpdate->id;
		$requestDataSession['orderType'] = 'New';
		\Session::put('rechargeOp',$requestDataSession);
		setcookie('rechargeOp', json_encode($requestDataSession));// set in cookie for session destroy [09.07.2016]
		//dd($CustomerId);
		//Cart::destroy($cartMaster->id);
		  if($request->mobik=='yes'){
		  	$ccavenueMerchantid = "mobik";
		  }
		return view('frontend.payment.redirect')
			->with('ccavenueUrl',$ccavenueUrl)
			->with('TransactionId',$TransactionId)
			//->with('TransactionValue',2)
			->with('mobidis', $mobidis)
			->with('TransactionValue',$cartMaster->grand_total)
			->with('ccavenueMerchantid',$ccavenueMerchantid); 		

	}
	
	
	

	 
		
	
}