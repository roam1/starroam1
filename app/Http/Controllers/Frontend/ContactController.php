<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ContactFormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\ContactUs;




class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(\Request::isMethod('post')) {
			echo '1';
			exit;
		}
		
    }
    
    //This code is being used for generating leads from Akbar Travels Microsite
    public function postIndex(ContactFormRequest $request) {
		//dd($request);
		
		$soapContact = new ContactUs;

			$data = ['CustomerName'=>$request->name,
				 	 'ContactNo'=>$request->contact_no,
				 	 'EmailId'=>$request->email,
				 	 'CountryofTravel'=>'112',
				 	 'Comment'=>Input::get('query'),
					];
			$formatdata = ['username'=>"ROAM1",
				 	 'password'=>"LeadTest2019",
		             'user_name'=>$request->name,
				 	 'user_mobile'=>$request->contact_no,
				 	 'user_email'=>$request->email,
				 	 'destination'=>$request->location,
				 	 'enq_src'=>'ROAM1',
					 'user_message '=>Input::get('query'),
					];
			
					//dd($formatdata);
			/* Akbar Lead Creation API Post request */		
	      $AkbarLeadAPIURL = "https://b2cbeta.akbartravels.com/holidays/api/lead_enquiry";
			
			$api = $soapContact->ContactUsCustomLeadRequest($AkbarLeadAPIURL,$formatdata) ;
			//dd($api);
			
			/* Roam1 API POst Inquire request */
			/*$soapContact->ContactUsEnquireRequest($data) ;
			
			if(!$soapContact->api_status){
				\Session::flash('message','Query Not Submitted Due to Some Error.');
				\Session::flash('alert-class', 'alert-error');
				return redirect()->route('value-added-services');	
			}else{*/
			
			$mailData = ['name' => $request->name,
						 'Email' => $request->email,
						 'Mobile' => $request->contact_no,
						 'country-name' => $request->country,
						 'query' =>Input::get('query')];
			$mailData['country-name'] = 'India';
			try{
			
			
			if (strpos(\URL::previous(), '/contact-us') !== false) {
				$email_subject = 'Akbar Travels Roam1 Contact for '.Input::get('enquiry_type').' enquiry';
				
			}
			$contact_us_CCes='';
			
			
			
		        
		           $support_email ="ajit.kumar@roam1.com";
		           $site_name ="roam1.com";
		           $contact_us_bcc="bipin.thakur@roam1.com";
		           $contact_us_CCes="";
		           $email_subject="Akbar Travels - Contacts";
			/* end of   set  Email (BCC/CC)  params as per  query type */
			\Mail::send('emails.getting-in-touch-admin', ['content' => $mailData] , function($message) use($support_email,$site_name,$email_subject,$contact_us_bcc,$contact_us_CCes)
			{
				$message->from("sales@roam1.com", "Roam1 Akbar Travels");
				//$message->to($customer_email)->cc($contact_us_CCes)->bcc
				$message->to("bipin.thakur@roam1.com")->cc("ajit.kumar@roam1.com")->bcc
				("deepak.kumar@roam1.com")->subject($email_subject);
				//$message->bcc($contact_us_bcc)->subject($email_subject);
			});
			
		
			} catch( SOAPFault $ex ) {
				return view('frontend.contactus.index', compact('breadcumbs','head_banner','mobile_home_banner','countries'))->with('cities', $cities)->withErrors($ex->getMessage());
			} 
	 $plan_key =  0;
	 $AllCounArray[$plan_key] = $this->getCountryList();
	foreach($AllCounArray as $allCn){
			if(!empty($allCn)){
				foreach($allCn as $allcountry){
					$AllCountries[$pl_key] = $allcountry;
					$pl_key++;
				}
			}
		}
			$countries = $AllCountries;
		    // return redirect()->back();
			return view('frontend.home.index',compact('api','countries'));
		//}
	 
	}

	protected function  getCountryList(){
	
			
			$soap_options =  \Config::get('custom.soap_options');
			
			
			try{
				$webservice = new \SoapClient('http://www.roam1.com/R1IP/Inquire.asmx?wsdl', $soap_options);
				
			
				$result = $webservice->GetCountries([
						'PartnerId'		=>	'371', 
					    'ClientIP'=>\Request::ip()
						
						]);	


				
				//dd($result);
			if($result->GetCountriesResult->Status==1):

				$ResultPlans=$result->GetCountriesResult->ListItem->Countries;

				if(isset($ResultPlans) && count($ResultPlans)>0){
					if(count($ResultPlans)>1){
						 foreach($ResultPlans as $eachPlan):
						     $dataInsert=json_decode(json_encode($eachPlan), true);
						   
                             // print_r($dataInsert);
						   /* insert  PriceforSort in system */
						  
							/* insert  PriceforSort in system */
						
						  $ResultCountry[]=$dataInsert;
						//print_r($ResultCountry);
						 endforeach;
					 }else{
						  $dataInsert=json_decode(json_encode($ResultPlans), true);
						  
						  
						  $ResultCountry[]=$dataInsert;
					 }
				}
			endif;	
			}catch(Exception $e){

			}

	return $ResultCountry;
	}
	
	
   

}
