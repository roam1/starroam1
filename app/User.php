<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract {

    //use Authenticatable, Authorizable, CanResetPassword;
    use Authenticatable,
        CanResetPassword,
        HasRoleAndPermission;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['FirstName', 'LastName', 'Email', 'Username', 'Password', 'api_password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    private $rules = [
        'FirstName' => 'required',
        //'LastName' => 'required',
        'Email' => 'required|email|unique:users',
        //'Username' => 'required|unique:users|min:6',
        'Username' => 'required|unique:users',
        'Password' => 'required'
    ];

    public function validate($data) {

        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            //dd($v->messages);
            $this->errors = $v->messages();
            return false;
        }
        // validation pass
        return true;
    }

    public function errors() {
        return $this->errors;
    }

    public function setPasswordAttribute($password) {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setApiPasswordAttribute($password) {
        $this->attributes['api_password'] = $password;
    }

    public function posts() {
        return $this->hasMany('App\Models\Post', 'author_id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function customer() {
        return $this->hasOne('App\Models\Customer', 'user_id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function profile() {
        return $this->hasOne('App\Models\UserImage');
    }

    static function getCustomerIdByEmail($email = null) {

        if (!empty($email)) {
           
            $user =User::with('customer')->where('email', '=', $email)->first();
            if (!empty($user) && isset($user->customer)) {

                
                return $user->customer->CustomerId;
            }
        }
        return null;
    }

    /*
     *  get Customer id by user id
     */

    static function getCustomerIdByUserId($userId) {

        $user = User::with('customer')->where('id', '=', $userId)->get(['id'])->first();
        if (isset($user->customer->CustomerId)) {
            return $user->customer->CustomerId;
        }
        return null;
    }

}
