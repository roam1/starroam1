<?php

use Illuminate\Database\Seeder;

class ErpSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('erp_settings')->insert(array(
        
		array(
			'id' => 1,
			'development_state' => 'uat',
			'inquiry_url' => 'http://www.roam1.com/R1IP_Test/Inquire.asmx?wsdl',
			'publish_url' => 'http://www.roam1.com/R1IP_Test/Publish.asmx?wsdl',
			'partner_id' => 371,
			'is_active' => 1,
			'created_at' => Carbon\Carbon::now(),
			'updated_at' => Carbon\Carbon::now()
		),
		array(
			'id' =>	2,
			'development_state' => 'production',
			'inquiry_url' => 'http://www.roam1.com/R1IP_Test/Inquire.asmx?wsdl',
			'publish_url' => 'http://www.roam1.com/R1IP_Test/Publish.asmx?wsdl',
			'partner_id' => 371,
			'is_active' => 0,
			'created_at' => Carbon\Carbon::now(),
			'updated_at' => Carbon\Carbon::now()
		)
		));
		
    }
}
