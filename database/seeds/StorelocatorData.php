<?php

use Illuminate\Database\Seeder;

class StorelocatorData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
          DB::table('storelocator')->insert([
            'country' => 'IN',
            'state' => 'West Bengal',
             'telephone'=> '9046952192',
               'status' =>1,
            'city' => 'Kolkata',
              
        ]);
        //
    }
}
