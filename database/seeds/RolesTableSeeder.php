<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        [
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Admin user can perferom any kind off task',
            'level' => 1,
            'created_at' => \Carbon::now(),
            'updated_at' => \Carbon::now()
        ],
        [
            'name' => 'Customer',
            'slug' => 'customer',
            'description' => 'Customer means fron-end user',
            'level' => 1,
            'created_at' => \Carbon::now(),
            'updated_at' => \Carbon::now(),
        ]]);
    }
}
