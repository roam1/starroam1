<?php

use Illuminate\Database\Seeder;

class ManagerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('managers')->insert([
            'name' => 'Supravat Mondal',
            'email' => 'supravat.mondal@bluehorse.in',
            'password' => bcrypt('password123'),
        ]);
    }
}
