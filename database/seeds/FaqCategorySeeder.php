<?php

use Illuminate\Database\Seeder;

class FaqCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
          DB::table('faq_category')->insert([
            'name' => 'Sample Category',
            'publish_date' => date("Y-m-d-h-i-s"),
             'is_active'=> 1,
			 'position'=>1,
             
        ]);
        //
    }
}
