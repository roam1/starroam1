<?php

use Illuminate\Database\Seeder;

class updateAdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
            'FirstName' => 'Rahul',
            'LastName' => 'Pramanik',
            'username' => 'superuser',
            'email' => 'Rahul.Pramanik@ivistasolutions.com',
            'password' => bcrypt('qt0D9W1O'),
            'created_at' => \Carbon::now(),
            'updated_at' => \Carbon::now()
            
        ],
        [
            'FirstName' => 'Amit',
            'LastName' => 'Bera',
            'username' => 'admin',
            'email' => 'amitberaasmita@gmail.com',
            'password' => bcrypt('qt0D9W1O'),
            'created_at' => \Carbon::now(),
            'updated_at' => \Carbon::now()
        ]]);
    }
}
