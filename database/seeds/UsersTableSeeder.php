<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        [
            'FirstName' => 'Supravat',
            'LastName' => 'Mondal',
            'username' => 'superuser',
            'email' => 'supravat.mondal@bluehorse.in',
            'password' => bcrypt('password123'),
            'created_at' => \Carbon::now(),
            'updated_at' => \Carbon::now()
            
        ],
        [
            'FirstName' => 'Amit',
            'LastName' => 'Bera',
            'username' => 'admin',
            'email' => 'amit.aera@bluehorse.in',
            'password' => bcrypt('password123'),
            'created_at' => \Carbon::now(),
            'updated_at' => \Carbon::now()
        ]]);
    }
}
