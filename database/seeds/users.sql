

INSERT INTO `users` (`id`, `FirstName`, `LastName`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `forgot_password_reset_time`, `forgot_password_token`, `api_password`) VALUES
(1, 'Rahul', 'Pramanik', 'superuser', 'Rahul.Pramanik@ivistasolutions.com', '$2y$10$mg7U3HfQflQT71IUhXtvceUTC08KVBRF7skNSEv4ntqPQ1Jwqnowq', '2MUx5uyfcsGCP4VIp2cNxNMP1t2A7fNebiCZLhYQAK6recuzOEe27UqIq065', '2016-03-09 12:07:14', '2016-03-09 12:18:51', '0000-00-00 00:00:00', '', ''),
(2, 'Amit', 'Bera', 'admin', 'amitberaasmita@gmail.com', '$2y$10$.7DzxdPc9AYqFVGabGJvLe4r20rIIorkZaNFSw92Y862mTFiqenUC', NULL, '2016-03-09 12:07:14', '2016-03-09 12:07:14', '0000-00-00 00:00:00', '', '');



INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2016-01-01 14:45:27', '2016-01-01 14:45:27'),
(2, 1, 2, '2016-01-01 14:45:27', '2016-01-01 14:45:27');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
