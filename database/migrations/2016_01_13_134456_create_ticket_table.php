<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='ticket';
	public function up()
    {
        Schema::dropIfExists($this->tableName);
		
		Schema::create($this->tableName, function(Blueprint $table){
		
			$table->increments('id');
			$table->string('PartnerId');
			$table->string('CustomerId');
			$table->string('Name');
			$table->string('Email');
			$table->string('ContactNo');
			$table->string('Subject');
			$table->text('Issue');
			$table->string('Category');
			$table->string('ClientIP');
			$table->string('Status');
			$table->string('ErrorMessage');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
