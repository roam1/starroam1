<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Storelocator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /* Create a table for storelocator */
        /* field:
         * country	varchar	255
`          state	varchar	255
           city	varchar	255
           pincode	varchar	255
           telephone	varchar	255
           address	text	
           lat	float	10,8
           long	float	10,8
           status	boolean		Yes
           publish_date	date		Yes
		
         * 
         */
        Schema::create('storelocator',function($table){
            $table->increments('id',15);
            $table->string('country',255);
            $table->string('state',255);
            $table->string('city',255);
            $table->string('telephone',255);
			$table->string('mobile',255);
            $table->longText('address');
            $table->float('lat',18,8);
            $table->float('long',18,8);
            $table->date('publish_date');
			$table->string('store_type',255);
			$table->string('business_enquiry_email',255);
			$table->string('customer_support_email',255);
            $table->timestamps();        
            $table->boolean('status')->default(0);
            $table->index('country');
            $table->index('state');
            $table->index('city');
            $table->index('telephone');
            $table->index('lat');
            $table->index('long');
            $table->index('publish_date');
            $table->index('status');
            
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          Schema::drop('storelocator');
    }
}
