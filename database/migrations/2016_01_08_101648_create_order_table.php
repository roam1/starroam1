<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	private $tableName='order';
    public function up()
    {
		Schema::dropIfExists($this->tableName);
		
		Schema::create($this->tableName, function(Blueprint $table){
		
			$table->increments('id');
			$table->text('SessionID');
			$table->text('CustomerID');
			$table->text('coupon_code');
			$table->string('cart_currency',25);
			$table->decimal('sub_total',20,4);
			$table->decimal('discount_amount',20,4);
			$table->decimal('delivery_charge',20,4);
			$table->decimal('tax_percnt',20,4);
			$table->decimal('tax',20,4);
			$table->decimal('grand_total',20,4);
			
			$table->string('address_type',50);
			$table->string('address_name',50);
			$table->string('address_email',50);
			$table->string('address_address',100);
			$table->string('address_landmark',100);
			$table->string('address_state',50);
			$table->string('address_pincode',50);
			$table->string('address_mobile',50);
			
			$table->string('TxStatus',50);
			$table->string('TxId',100);
			$table->string('TxRefNo',100);
			$table->string('pgTxnNo',100);
			$table->string('pgRespCode',100);
			$table->string('TxMsg',100);
			$table->string('amount',100);
			$table->string('authIdCode',100);
			$table->string('issuerRefNo',100);
			$table->string('signature',100);
			
			$table->string('transactionId',100);
			$table->string('paymentMode',100);
			$table->string('TxGateway',100);
			$table->string('currency',100);
			$table->string('maskedCardNumber',100);
			$table->string('cardType',100);
			$table->string('cardHolderName',100);
			
			$table->string('firstName',100);
			$table->string('lastName',100);
			$table->string('email',100);
			$table->string('mobileNo',100);
			$table->string('txnDateTime',100);
			
			$table->string('soap_status',100);
			$table->string('soap_error_message',100);
			$table->string('soap_order_id',100);
			
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
