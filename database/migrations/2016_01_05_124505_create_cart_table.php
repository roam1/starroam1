<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	private $tableName='cart';
    public function up()
    {
		Schema::dropIfExists($this->tableName);
		
		Schema::create($this->tableName, function(Blueprint $table){
		
			$table->increments('id');
			$table->text('SessionID');
			$table->text('CustomerID');
			$table->string('txnId',100);
			$table->text('coupon_code');
			$table->string('Currency',25);
			$table->decimal('sub_total',20,4);
			$table->decimal('discount_amount',20,4);
			$table->decimal('delivery_charge',20,4);
			$table->decimal('tax_percnt',20,4);
			$table->decimal('tax',20,4);
			$table->decimal('grand_total',20,4);
			$table->string('address_type',50);
			$table->string('address_name',50);
			$table->string('address_email',50);
			$table->string('address_address',100);
			$table->string('address_landmark',100);
			$table->string('address_state',50);
			$table->string('address_pincode',50);
			$table->string('address_mobile',50);
			
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
