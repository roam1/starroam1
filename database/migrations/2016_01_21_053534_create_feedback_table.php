<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	private $tableName='feedback';
    public function up()
    {
        Schema::dropIfExists($this->tableName);
		
		Schema::create($this->tableName, function(Blueprint $table){
		
			$table->increments('id');
			$table->string('email');
			$table->string('product');
			$table->string('purchase_exp_rate');
			$table->text('purchase_exp_comment');
			$table->string('product_exp_rate');
			$table->text('product_exp_comment');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
