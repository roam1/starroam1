<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::dropIfExists('transactions');
		
        Schema::create('transactions', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('TxId',100)->unique();
			$table->string('TxStatus',100);
			$table->string('TxRefNo',100);
			$table->string('PaymentType',100);
			$table->integer('pgRespCode');
			$table->decimal('amount',20,4);
			$table->string('mobileNo',255);
			$table->boolean('Status');
			$table->text('ErrorMessage');
			$table->string('PaymentReferenceNo',100);
			$table->longText('transaction_info');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
