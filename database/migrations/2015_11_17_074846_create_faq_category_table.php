<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_category', function (Blueprint $table) {
            $table->increments('id');
			$table->string('page_code');
            $table->string('name');
            $table->timestamp('publish_date');
            $table->enum('is_active',['0','1']);
            $table->smallInteger('position');
            $table->timestamps();         
            $table->index(['name']);
            $table->index(['position']);
            $table->index('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faq_category');
    }
}
