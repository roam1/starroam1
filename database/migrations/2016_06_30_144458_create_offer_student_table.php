<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_student', function (Blueprint $table) {
            $table->increments('id');
			$table->string('country')->nullable()->default(null);
			$table->string('plan')->nullable()->default(null);
			$table->string('title')->nullable()->default(null);
			/*$table->string('meta_title');
			$table->text('meta_description');*/
			$table->string('image')->nullable()->default(null);
			$table->text('banner_text')->nullable()->default(null);
			$table->string('mobile_image')->nullable()->default(null);
			$table->text('mobile_banner_text')->nullable()->default(null);
			$table->text('other_content')->nullable()->default(null);
			$table->smallInteger('position');
			$table->enum('status',['0','1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_student');
    }
}
