<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setting', function (Blueprint $table) {
            $table->increments('id');
			$table->string('attribute_code')->unique();
			$table->string('attribute_name');
			$table->string('attribute_value');
            $table->timestamps();
			$table->index(['attribute_code']);
			$table->index(['attribute_name']);
			$table->index(['attribute_value']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_setting');
    }
}
