<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pincodechecker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='pincodechecker'; 
    
    public function up()
    {
		Schema::dropIfExists($this->tableName);
       	Schema::create($this->tableName,function(Blueprint $table){
			  $table->increments('id');
			  $table->string('address',255);
			  $table->string('city',255);
			  $table->string('state',255);
			  $table->string('delivery_days',255);
			  $table->timestamps();
			 /* add colums index */
			 $table->index(['address']);
             $table->index(['city']);
             $table->index(['state']);
             $table->index(['delivery_days']);
          
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         //
        Schema::dropIfExists($this->tableName);
    }
}
