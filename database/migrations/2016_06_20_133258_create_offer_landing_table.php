<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferLandingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_landing', function (Blueprint $table) {
            $table->increments('id');
			$table->string('offer_heading');
			$table->string('image');
			$table->string('mobile_image');
			$table->text('content');
			$table->string('url');
			$table->string('plan_type');
			$table->string('validity');
			$table->date('exp_date');
			$table->smallInteger('position');
			$table->enum('status',['0','1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_landing');
    }
}
