<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewAdvantage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advantage_new', function (Blueprint $table) {
            $table->increments('id');
			$table->string('page_name')->nullable()->default(null);
			$table->string('title')->nullable()->default(null);
			$table->string('image')->nullable()->default(null);
			$table->text('banner_text')->nullable()->default(null);
			$table->string('mobile_image')->nullable()->default(null);
			$table->text('mobile_banner_text')->nullable()->default(null);
			$table->string('icon_image')->nullable()->default(null);
			$table->text('other_content')->nullable()->default(null);
			$table->smallInteger('position');
			$table->enum('status',['0','1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advantage_new');
    }
}
