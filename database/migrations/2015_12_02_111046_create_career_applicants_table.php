<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_applicants', function (Blueprint $table) {
            $table->increments('id')->unsigned();
			$table->integer('job_id')->unsigned();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('phone');
			$table->string('resume');
			$table->text('content');
            $table->timestamps();
			$table->index(['first_name']);
            $table->index(['last_name']);
			$table->index(['email']);
			$table->index(['phone']);
			$table->foreign('job_id')
				  ->references('id')
				  ->on('careers')
				  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('career_applicants');
    }
}
