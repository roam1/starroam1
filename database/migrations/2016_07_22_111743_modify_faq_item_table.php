<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyFaqItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_category', function (Blueprint $table) {
            $table->increments('id');
			$table->string('page_code');
            $table->string('name');
            $table->timestamp('publish_date');
            $table->enum('is_active',['0','1']);
            $table->smallInteger('position');
            $table->timestamps();         
        });
		Schema::create('faq_item', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('page_code');
            $table->text('question');
            $table->text('answer');
            $table->smallInteger('position');
            $table->enum('is_active',['0','1']);
            $table->timestamp('publish_date');
            $table->timestamps();        
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faq_item');
		Schema::drop('faq_category');
    }
}
