<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visits', function (Blueprint $table) {
			  $table->string('matchtype')->after('referral')->nullable()->default(null);
			  $table->string('network')->after('matchtype')->nullable()->default(null);
			  $table->string('device')->after('network')->nullable()->default(null);
			  $table->string('devicemodel')->after('device')->nullable()->default(null);
			  $table->string('creative')->after('devicemodel')->nullable()->default(null);
			  $table->string('target')->after('creative')->nullable()->default(null);
			  $table->string('adposition')->after('target')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
