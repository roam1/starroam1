<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsBlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_block', function (Blueprint $table) {
            $table->increments('id');
			$table->string('identifier')->unique();
			$table->string('title');
			$table->text('content');
            $table->timestamps();
			$table->index(['title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_block');
    }
}
