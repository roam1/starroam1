<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            
        Schema::create('careers', function (Blueprint $careers) {
            $careers->increments('id');
            $careers->string('job_code',255)->unique();;
            $careers->string('job_position',255);
            $careers->string('location',255);
            $careers->string('department',255);
            $careers->string('experience',255);
            $careers->mediumText('mendatory_skill');
            $careers->mediumText('desirable_skill');
            $careers->mediumText('job_description');
            $careers->longText('other_requirements');
            $careers->timestamp('posting_on');
            $careers->enum('is_active',['0','1']);
            $careers->smallInteger('position');
            $careers->timestamps();         
            $careers->index(['job_position']);
            $careers->index(['location']);
            $careers->index(['department']);
            $careers->index(['position']);
            $careers->index('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('careers');
    }
}
