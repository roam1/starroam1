<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title');
			$table->string('slug')->unique();
			$table->string('image');
			$table->text('content');
			$table->string('meta_keyword');
			$table->string('meta_title');
			$table->string('meta_description');
			$table->smallInteger('position');
            $table->enum('status',['0','1']);
            $table->timestamps();
			$table->index(['title']);
			$table->index(['slug']);
			$table->index(['meta_keyword']);
			$table->index(['meta_title']);
			$table->index(['meta_description']);
            $table->index(['position']);
            $table->index(['status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms');
    }
}
