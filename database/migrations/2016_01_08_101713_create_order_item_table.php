<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	private $tableName='order_item';
    public function up()
    {
		Schema::dropIfExists($this->tableName);
		
		Schema::create($this->tableName, function(Blueprint $table){
	    
			  $table->increments('id');
			  $table->integer('order_id')->unsigned();
			  $table->bigInteger('PlanId');
			  $table->string('PlanName');
			  $table->text('PlanDescription');
			  $table->decimal('Price',20,4);
			  $table->string('Currency',25);
			  $table->string('TravelPurpose',200);
			  $table->integer('Validity');
			  $table->decimal('LocalCalls',20,4);
			  $table->decimal('CallToIndia',20,4);
			  $table->decimal('SMS',20,4);
			  $table->decimal('Data',20,8);
			  $table->string('DataUnit',25);
			  $table->decimal('MMS',20,5);
			  $table->text('Freebies');
			  $table->integer('Quantity');
			  $table->integer('Discount');
			  $table->integer('ItemTotal');
			  $table->text('SessionID');
			  $table->text('CustomerID');
			  $table->timestamps();
			  $table->foreign('order_id')->references('id')->on('order')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
