<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
		
		Schema::create('blog_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('publish_date');
            $table->enum('is_active',['0','1']);
            $table->smallInteger('position');
            $table->timestamps();         
            $table->index(['name']);
            $table->index(['position']);
            $table->index('is_active');
        });
        
        Schema::create('blog_posts', function (Blueprint $table) {
			
			$table->increments('id',true)->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('author_id')->unsigned();
            $table->string('url',255);
            $table->string('title',255);
            $table->string('description',255);
            $table->longText('content');
            $table->string('image',255);
            $table->smallInteger('position')->default(0);
            $table->boolean('status')->default(0);
            $table->timestamps();   

            $table->index('title');
            $table->index('category_id');
            $table->index('url');
            $table->index('description');
            $table->index('position');
            $table->index('status');
			$table->foreign('category_id')
				  ->references('id')
				  ->on('blog_categories')
				  ->onDelete('cascade');
			$table->foreign('author_id')
				  ->references('id')
				  ->on('users')
				  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_posts');
        Schema::drop('blog_categories');
    }
}
