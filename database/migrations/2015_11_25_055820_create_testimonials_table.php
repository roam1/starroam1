<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id')->unsigned();
			$table->integer('TestimonialId')->comment('Testimonial ID from API Key'); //entity_id => testimonial id fetch through API key
            $table->string('CustomerName');
			$table->string('CompanyName');
			$table->string('Designation');
			$table->text('Testimonial');
			$table->timestamp('TestimonialDate');
			$table->string('TestimonialImage');
			$table->boolean('Highlight');            
			$table->smallInteger('position');
            $table->timestamps();        
            $table->index(['CustomerName']);
			$table->index(['CompanyName']);
            $table->index(['position']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('testimonials');
    }
}
