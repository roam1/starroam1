/* new plan filter js [29.07.2016]*/
$('#filter-country').change(function () {
	$('.plan-loader, .plan-overlay').show();
	$('body').css('overflow','hidden');
	$("#filter-duration").find('option:first').attr('selected', 'selected');
	$("#filter-type").find('option:first').attr('selected', 'selected');
	$("#filter-network").find('option:first').attr('selected', 'selected');
	/* country for url */
	var country = $("#filter-country").find("option:selected").text().toLowerCase();
	country = country.replace(/ /g, "-");
	if(country == 'united-states'){ country = 'usa'; }
	if(country == 'united-kingdom'){ country = 'uk'; }
	if(country == 'hong-kong'){ country = 'hong-kong'; }
	if(country == 'uae'){ country = 'uae'; }
	if(country == 'united-arab-emirates'){ country = 'uae'; }
	window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country);
	/* country for url */
	//alert(document.domain);
    url = jQuery('#filter-link').attr('data-url');
    $.get(url,
            {country: $('#filter-country').val(),duration: $('#filter-duration').val(),type: $('#filter-type').val(),network: $('#filter-network').val()},
            function (data) {
				$('.plan-loader, .plan-overlay').hide();
				$('body').css('overflow','');
				$("#filter-body").empty();
				$("#plan_filter_banner").empty();
				$("#plan-content").empty();
				$("#filter-content").empty();
				
				$('#filter-network').empty().append(data.NetworkFilter);
				$('#filter-type').empty().append(data.TypeFilter);
				$('#filter-duration').empty().append(data.DurationFilter);
				
				$("#plan-content").hide();
				$("#filter-content").show();
                $("#filter-content").append(data.filter_plan_content);
				
				$("#plan_banner").hide();
				$("#plan_filter_banner").show();
                $("#plan_filter_banner").append(data.filter_banner);
				
				$("#normal-body").hide();
				$( "#normal-body" ).empty();
				$("#filter-body").show();
                $("#filter-body").append(data.filter_layout);
				
				$("#mobile-filter-body").empty();
				$("#mobile-plan-body").hide();
				$("#mobile-plan-body").removeClass('hidden-sm  hidden-md hidden-lg');
				$("#mobile-filter-body").show();
                $("#mobile-filter-body").append(data.mobile_filter_layout);
				
				$( "#breadcumbs" ).empty();
				$("#breadcumbs").append(data.filter_breadcumb);
				
				$('title').html(data.plan_meta_title);
				$('meta[name=description]').attr('content', data.plan_meta_description);
				$('meta[name=keywords]').attr('content', data.plan_meta_keywords);
            });
});


$('#filter-duration').change(function () {
	$('.plan-loader, .plan-overlay').show();
	$('body').css('overflow','hidden');
	/* country for url */
	var country = $("#filter-country").find("option:selected").text().toLowerCase();
	country = country.replace(/ /g, "-");
	if(country == 'united-states'){ country = 'usa'; }
	if(country == 'united-kingdom'){ country = 'uk'; }
	if(country == 'hong-kong'){ country = 'hong-kong'; }
	if(country == 'uae'){ country = 'uae'; }
	if(country == 'united-arab-emirates'){ country = 'uae'; }
	var duration = type = network ='';
	/* Duration for url */
	if($("#filter-duration").find("option:selected").val()!= ''){
		duration = 'duration='+$("#filter-duration").find("option:selected").val();
	}
	/* type for url */
	if($("#filter-type").find("option:selected").val()!= ''){
		type = 'type='+$("#filter-type").find("option:selected").val();
	}
	/* network for url */
	if($("#filter-network").find("option:selected").val()!= ''){
		network = 'network='+$("#filter-network").find("option:selected").val();
	}
	if(duration != '' || type != '' || network != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration+'&'+type+'&'+network);	
	}else if(duration != '' && type != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration+'&'+type);	
	}else if(duration != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration);	
	}else{
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country);
	}
	
	/* country for url */
	
    url = jQuery('#filter-link').attr('data-url');
    $.get(url,
            {country: $('#filter-country').val(),duration: $('#filter-duration').val(),type: $('#filter-type').val(),network: $('#filter-network').val()},
            function (data) {
				$('.plan-loader, .plan-overlay').hide();
				$('body').css('overflow','');
				$("#filter-body").empty();
				$("#normal-body").hide();
				$("#filter-body").show();
                $("#filter-body").append(data.filter_layout);
				
				$("#mobile-filter-body").empty();
				$("#mobile-plan-body").hide();
				$("#mobile-plan-body").removeClass('hidden-sm  hidden-md hidden-lg');
				$("#mobile-filter-body").show();
                $("#mobile-filter-body").append(data.mobile_filter_layout);
            });
});


$('#filter-type').change(function () {
	$('.plan-loader, .plan-overlay').show();
	$('body').css('overflow','hidden');
	/* country for url */
	var country = $("#filter-country").find("option:selected").text().toLowerCase();
	country = country.replace(/ /g, "-");
	if(country == 'united-states'){ country = 'usa'; }
	if(country == 'united-kingdom'){ country = 'uk'; }
	if(country == 'hong-kong'){ country = 'hong-kong'; }
	if(country == 'uae'){ country = 'uae'; }
	if(country == 'united-arab-emirates'){ country = 'uae'; }
	var duration = type = network ='';
	/* Duration for url */
	if($("#filter-duration").find("option:selected").val()!= ''){
		duration = 'duration='+$("#filter-duration").find("option:selected").val();
	}
	/* type for url */
	if($("#filter-type").find("option:selected").val()!= ''){
		type = 'type='+$("#filter-type").find("option:selected").val();
	}
	/* network for url */
	if($("#filter-network").find("option:selected").val()!= ''){
		network = 'network='+$("#filter-network").find("option:selected").val();
	}
	if(duration != '' || type != '' || network != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration+'&'+type+'&'+network);	
	}else if(duration != '' && type != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration+'&'+type);	
	}else if(duration != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration);	
	}else{
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country);
	}
	
	/* country for url */
    url = jQuery('#filter-link').attr('data-url');
    $.get(url,
            {country: $('#filter-country').val(),duration: $('#filter-duration').val(),type: $('#filter-type').val(),network: $('#filter-network').val()},
            function (data) {
				$('.plan-loader, .plan-overlay').hide();
				$('body').css('overflow','');
				$("#filter-body").empty();
				$("#normal-body").hide();
				$("#filter-body").show();
                $("#filter-body").append(data.filter_layout);
				
				$("#mobile-filter-body").empty();
				$("#mobile-plan-body").hide();
				$("#mobile-plan-body").removeClass('hidden-sm  hidden-md hidden-lg');
				$("#mobile-filter-body").show();
                $("#mobile-filter-body").append(data.mobile_filter_layout);
            });
});


$('#filter-network').change(function () {
	$('.plan-loader, .plan-overlay').show();
	$('body').css('overflow','hidden');
	/* country for url */
	var country = $("#filter-country").find("option:selected").text().toLowerCase();
	country = country.replace(/ /g, "-");
	if(country == 'united-states'){ country = 'usa'; }
	if(country == 'united-kingdom'){ country = 'uk'; }
	if(country == 'hong-kong'){ country = 'hong-kong'; }
	if(country == 'uae'){ country = 'uae'; }
	if(country == 'united-arab-emirates'){ country = 'uae'; }
	var duration = type = network ='';
	/* Duration for url */
	if($("#filter-duration").find("option:selected").val()!= ''){
		duration = 'duration='+$("#filter-duration").find("option:selected").val();
	}
	/* type for url */
	if($("#filter-type").find("option:selected").val()!= ''){
		type = 'type='+$("#filter-type").find("option:selected").val();
	}
	/* network for url */
	if($("#filter-network").find("option:selected").val()!= ''){
		network = 'network='+$("#filter-network").find("option:selected").val();
	}
	if(duration != '' || type != '' || network != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration+'&'+type+'&'+network);	
	}else if(duration != '' && type != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration+'&'+type);	
	}else if(duration != ''){
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country+'?'+duration);	
	}else{
		window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country);
	}
	
	/* country for url */
    url = jQuery('#filter-link').attr('data-url');
    $.get(url,
            {country: $('#filter-country').val(),duration: $('#filter-duration').val(),type: $('#filter-type').val(),network: $('#filter-network').val()},
            function (data) {
				$('.plan-loader, .plan-overlay').hide();
				$('body').css('overflow','');
				$("#filter-body").empty();
				$("#normal-body").hide();
				$("#filter-body").show();
                $("#filter-body").append(data.filter_layout);
				
				$("#mobile-filter-body").empty();
				$("#mobile-plan-body").hide();
				$("#mobile-plan-body").removeClass('hidden-sm  hidden-md hidden-lg');
				$("#mobile-filter-body").show();
                $("#mobile-filter-body").append(data.mobile_filter_layout);
            });
});
$('#filter-clear, #mob-filter-clear').click(function () {
	$('.plan-loader, .plan-overlay').show();
	$('body').css('overflow','hidden');
	$("#filter-duration").find('option:first').attr('selected', 'selected');
	$("#filter-type").find('option:first').attr('selected', 'selected');
	$("#filter-network").find('option:first').attr('selected', 'selected');
	/* country for url */
	var country = $("#filter-country").find("option:selected").text().toLowerCase();
	country = country.replace(/ /g, "-");
	if(country == 'united-states'){ country = 'usa'; }
	if(country == 'united-kingdom'){ country = 'uk'; }
	if(country == 'hong-kong'){ country = 'hong-kong'; }
	if(country == 'uae'){ country = 'uae'; }
	if(country == 'united-arab-emirates'){ country = 'uae'; }
	window.history.pushState('obj', 'newtitle','http://localhost/roam1'+/international-sim-cards-offers/'+country);
	/* country for url */
	//alert(document.domain);
    url = jQuery('#filter-link').attr('data-url');
    $.get(url,
            {country: $('#filter-country').val(),duration: $('#filter-duration').val(),type: $('#filter-type').val(),network: $('#filter-network').val()},
            function (data) {
				$('.plan-loader, .plan-overlay').hide();
				$('body').css('overflow','');
				$("#filter-body").empty();
				$('#filter-network').empty().append(data.NetworkFilter);
				$('#filter-type').empty().append(data.TypeFilter);
				$('#filter-duration').empty().append(data.DurationFilter);
				$("#normal-body").hide();
				$("#filter-body").show();
                $("#filter-body").append(data.filter_layout);
				
				$("#mobile-filter-body").empty();
				$("#mobile-plan-body").hide();
				$("#mobile-plan-body").removeClass('hidden-sm  hidden-md hidden-lg');
				$("#mobile-filter-body").show();
                $("#mobile-filter-body").append(data.mobile_filter_layout);
            });
});
/* sticky menu */
$(window).on("scroll", function() {
 //if($(window).width()>1000){
var community = $('.filter-border').height()+580;
//var seccom = $('.listing-page').offset().top;
var scrollDown = $(window).scrollTop();
//$('#right-panel').addClass('right-view');
var footerTop = $('.faqPage').offset().top-($(window).height()-260);

if(community<=scrollDown){
if(footerTop<scrollDown){
var limitVar = footerTop-scrollDown;
$('.planStaticHead').css('position','fixed');
//$('.planStaticHead').css('right','6.6%');
$('.planStaticHead').css('top',limitVar);
}else{
$('.planStaticHead').css('position','fixed');
//$('.planStaticHead').css('right','6.6%');
$('.planStaticHead').css('width',$(".plan-main-area").width());
$('.planStaticHead').css('top','0');
}
}else{
$('.planStaticHead').css('position','');
//$('.planStaticHead').css('right','');
$('.planStaticHead').css('top','');
}
 
// }
}); 

/* old js */
function addToBagPlan(formId){
	var Applyform='addToPlanForm'+formId;
	$('#FinalPlanBuyForm  input[name=addToPlanFormId]' ).val(Applyform);
	$('#pincode-pop').modal('show');
}
function FinalPlanBuy(){
	$(".pincode-pop #load-active").css("display","block");
	//var GetFormId=$('#FinalPlanBuyForm input[name=addToPlanFormId]' ).val();
		$.ajax({
				url: $('form#FinalPlanBuyForm').attr('action'),
				type: 'post',
				cache: false,
				dataType: 'json',
				data: $('form#FinalPlanBuyForm').serialize(),
				beforeSend: function() { 
					$("#validation-errors").hide().empty(); 
				},
				success: function(data) {
					if(data.success==true){
					console.log("Success!");
						$(".pincode-pop #load-active").css("display","none");
						if(data.pincode.delivery_days == '6 hours'){
							$("#deliverySuccess .msg-error").html('Delivery within '+data.pincode.delivery_days+'. For orders before 11 AM only.'+"<br>"+' Orders after 11 AM will be delivered next working day.');
						}else{
							$("#deliverySuccess .msg-error").text('Delivery within '+data.pincode.delivery_days+'.');
						}
						$("#deliveryError").hide();
						$("#deliverySuccess").show();
					}
					else{
						$(".pincode-pop #load-active").css("display","none");
						$("#deliverySuccess").hide();
						$("#deliveryError").show();
					}
				},
				error: function(xhr, textStatus, thrownError) {
					alert('Something went to wrong.Please Try again later...');
				}
			});

	
	
	//document.getElementById(document.getElementById("FinalPlanBuyForm").elements.namedItem("addToPlanFormId").value).submit();
	
}
function byThisPlanNow(){document.getElementById(document.getElementById("FinalPlanBuyForm").elements.namedItem("addToPlanFormId").value).submit();}
$(".checkboxUrl" ).on( "click", function() {

	window.location.href=$(this).attr('data-filter');
});