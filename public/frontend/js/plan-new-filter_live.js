function addToBagPlan(e) {
    var t = "addToPlanForm" + e;
    $("#FinalPlanBuyForm  input[name=addToPlanFormId]").val(t), $("#pincode-pop").modal("show")
}

function FinalPlanBuy() {
    $(".pincode-pop #load-active").css("display", "block"), $.ajax({
        url: $("form#FinalPlanBuyForm").attr("action"),
        type: "post",
        cache: !1,
        dataType: "json",
        data: $("form#FinalPlanBuyForm").serialize(),
        beforeSend: function() {},
        success: function(e) {
            1 == e.success ? (console.log("Success!"), $(".pincode-pop #load-active").css("display", "none"), 1 == e.pincode.CheckExpressDeliveryResult.ListItem.Cities.IsExpress ? $("#deliverySuccess .msg-error").html("Delivery will be done on the same day for orders placed before 9 AM only.<br> Orders after 9 AM will be delivered next working day.") : $("#deliverySuccess .msg-error").text("Delivery within 48 hours."), $("#deliveryError").hide(), $("#deliverySuccess").show()) : ($(".pincode-pop #load-active").css("display", "none"), $("#deliverySuccess").hide(), $("#deliveryError").show())
        },
        error: function(e, t, o) {
            alert("Something went to wrong.Please Try again later...")
        }
    })
}

function byThisPlanNow() {
    document.getElementById(document.getElementById("FinalPlanBuyForm").elements.namedItem("addToPlanFormId").value).submit()
}
$("#filter-country").change(function() {
    $(".plan-loader, .plan-overlay").show(), $("body").css("overflow", "hidden"), $("#filter-duration").find("option:first").attr("selected", "selected"), $("#filter-type").find("option:first").attr("selected", "selected"), $("#filter-network").find("option:first").attr("selected", "selected");
    var e = $("#filter-country").find("option:selected").text().toLowerCase();
    "united-states" == (e = e.replace(/ /g, "-")) && (e = "usa"), "united-kingdom" == e && (e = "uk"), "hong-kong" == e && (e = "hong-kong"), "uae" == e && (e = "uae"), "united-arab-emirates" == e && (e = "uae"), window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card"), url = jQuery("#filter-link").attr("data-url"), $.get(url, {
        country: $("#filter-country").val(),
        duration: $("#filter-duration").val(),
        type: $("#filter-type").val(),
        network: $("#filter-network").val()
    }, function(e) {
        $(".plan-loader, .plan-overlay").hide(), $("body").css("overflow", ""), $("#filter-body").empty(), $("#plan_filter_banner").empty(), $("#plan-content").empty(), $("#filter-content").empty(), $("#filter-network").empty().append(e.NetworkFilter), $("#filter-type").empty().append(e.TypeFilter), $("#filter-duration").empty().append(e.DurationFilter), $("#plan-content").hide(), $("#filter-content").show(), $("#filter-content").append(e.filter_plan_content), $("#plan_banner").hide(), $("#plan_filter_banner").show(), $("#plan_filter_banner").append(e.filter_banner), $("#normal-body").hide(), $("#normal-body").empty(), $("#filter-body").show(), $("#filter-body").append(e.filter_layout), $("#mobile-filter-body").empty(), $("#mobile-plan-body").hide(), $("#mobile-plan-body").removeClass("hidden-sm  hidden-md hidden-lg"), $("#mobile-filter-body").show(), $("#mobile-filter-body").append(e.mobile_filter_layout), $("#breadcumbs").empty(), $("#breadcumbs").append(e.filter_breadcumb)
    })
}), $("#filter-duration").change(function() {
    $(".plan-loader, .plan-overlay").show(), $("body").css("overflow", "hidden");
    var e = $("#filter-country").find("option:selected").text().toLowerCase();
    "united-states" == (e = e.replace(/ /g, "-")) && (e = "usa"), "united-kingdom" == e && (e = "uk"), "hong-kong" == e && (e = "hong-kong"), "uae" == e && (e = "uae"), "united-arab-emirates" == e && (e = "uae");
    var t = type = network = "";
    "" != $("#filter-duration").find("option:selected").val() && (t = "duration=" + $("#filter-duration").find("option:selected").val()), "" != $("#filter-type").find("option:selected").val() && (type = "type=" + $("#filter-type").find("option:selected").val()), "" != $("#filter-network").find("option:selected").val() && (network = "network=" + $("#filter-network").find("option:selected").val()), "" != t || "" != type || "" != network ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t + "&" + type + "&" + network) : "" != t && "" != type ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t + "&" + type) : "" != t ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t) : window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card"), url = jQuery("#filter-link").attr("data-url"), $.get(url, {
        country: $("#filter-country").val(),
        duration: $("#filter-duration").val(),
        type: $("#filter-type").val(),
        network: $("#filter-network").val()
    }, function(e) {
        $(".plan-loader, .plan-overlay").hide(), $("body").css("overflow", ""), $("#filter-body").empty(), $("#normal-body").hide(), $("#filter-body").show(), $("#filter-body").append(e.filter_layout), $("#mobile-filter-body").empty(), $("#mobile-plan-body").hide(), $("#mobile-plan-body").removeClass("hidden-sm  hidden-md hidden-lg"), $("#mobile-filter-body").show(), $("#mobile-filter-body").append(e.mobile_filter_layout)
    })
}), $("#filter-type").change(function() {
    $(".plan-loader, .plan-overlay").show(), $("body").css("overflow", "hidden");
    var e = $("#filter-country").find("option:selected").text().toLowerCase();
    "united-states" == (e = e.replace(/ /g, "-")) && (e = "usa"), "united-kingdom" == e && (e = "uk"), "hong-kong" == e && (e = "hong-kong"), "uae" == e && (e = "uae"), "united-arab-emirates" == e && (e = "uae");
    var t = type = network = "";
    "" != $("#filter-duration").find("option:selected").val() && (t = "duration=" + $("#filter-duration").find("option:selected").val()), "" != $("#filter-type").find("option:selected").val() && (type = "type=" + $("#filter-type").find("option:selected").val()), "" != $("#filter-network").find("option:selected").val() && (network = "network=" + $("#filter-network").find("option:selected").val()), "" != t || "" != type || "" != network ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t + "&" + type + "&" + network) : "" != t && "" != type ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t + "&" + type) : "" != t ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t) : window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card"), url = jQuery("#filter-link").attr("data-url"), $.get(url, {
        country: $("#filter-country").val(),
        duration: $("#filter-duration").val(),
        type: $("#filter-type").val(),
        network: $("#filter-network").val()
    }, function(e) {
        $(".plan-loader, .plan-overlay").hide(), $("body").css("overflow", ""), $("#filter-body").empty(), $("#normal-body").hide(), $("#filter-body").show(), $("#filter-body").append(e.filter_layout), $("#mobile-filter-body").empty(), $("#mobile-plan-body").hide(), $("#mobile-plan-body").removeClass("hidden-sm  hidden-md hidden-lg"), $("#mobile-filter-body").show(), $("#mobile-filter-body").append(e.mobile_filter_layout)
    })
}), $("#filter-network").change(function() {
    $(".plan-loader, .plan-overlay").show(), $("body").css("overflow", "hidden");
    var e = $("#filter-country").find("option:selected").text().toLowerCase();
    "united-states" == (e = e.replace(/ /g, "-")) && (e = "usa"), "united-kingdom" == e && (e = "uk"), "hong-kong" == e && (e = "hong-kong"), "uae" == e && (e = "uae"), "united-arab-emirates" == e && (e = "uae");
    var t = type = network = "";
    "" != $("#filter-duration").find("option:selected").val() && (t = "duration=" + $("#filter-duration").find("option:selected").val()), "" != $("#filter-type").find("option:selected").val() && (type = "type=" + $("#filter-type").find("option:selected").val()), "" != $("#filter-network").find("option:selected").val() && (network = "network=" + $("#filter-network").find("option:selected").val()), "" != t || "" != type || "" != network ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t + "&" + type + "&" + network) : "" != t && "" != type ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t + "&" + type) : "" != t ? window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card?" + t) : window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card"), url = jQuery("#filter-link").attr("data-url"), $.get(url, {
        country: $("#filter-country").val(),
        duration: $("#filter-duration").val(),
        type: $("#filter-type").val(),
        network: $("#filter-network").val()
    }, function(e) {
        $(".plan-loader, .plan-overlay").hide(), $("body").css("overflow", ""), $("#filter-body").empty(), $("#normal-body").hide(), $("#filter-body").show(), $("#filter-body").append(e.filter_layout), $("#mobile-filter-body").empty(), $("#mobile-plan-body").hide(), $("#mobile-plan-body").removeClass("hidden-sm  hidden-md hidden-lg"), $("#mobile-filter-body").show(), $("#mobile-filter-body").append(e.mobile_filter_layout)
    })
}), $("#filter-clear, #mob-filter-clear").click(function() {
    $(".plan-loader, .plan-overlay").show(), $("body").css("overflow", "hidden"), $("#filter-duration").find("option:first").attr("selected", "selected"), $("#filter-type").find("option:first").attr("selected", "selected"), $("#filter-network").find("option:first").attr("selected", "selected");
    var e = $("#filter-country").find("option:selected").text().toLowerCase();
    "united-states" == (e = e.replace(/ /g, "-")) && (e = "usa"), "united-kingdom" == e && (e = "uk"), "hong-kong" == e && (e = "hong-kong"), "uae" == e && (e = "uae"), "united-arab-emirates" == e && (e = "uae"), window.history.pushState("obj", "newtitle", "http://" + document.domain + "/" + e + "-international-sim-card"), url = jQuery("#filter-link").attr("data-url"), $.get(url, {
        country: $("#filter-country").val(),
        duration: $("#filter-duration").val(),
        type: $("#filter-type").val(),
        network: $("#filter-network").val()
    }, function(e) {
        $(".plan-loader, .plan-overlay").hide(), $("body").css("overflow", ""), $("#filter-body").empty(), $("#filter-network").empty().append(e.NetworkFilter), $("#filter-type").empty().append(e.TypeFilter), $("#filter-duration").empty().append(e.DurationFilter), $("#normal-body").hide(), $("#filter-body").show(), $("#filter-body").append(e.filter_layout), $("#mobile-filter-body").empty(), $("#mobile-plan-body").hide(), $("#mobile-plan-body").removeClass("hidden-sm  hidden-md hidden-lg"), $("#mobile-filter-body").show(), $("#mobile-filter-body").append(e.mobile_filter_layout)
    })
}), $(window).on("scroll", function() {
    var e = $(".filter-border").height() + 580,
        t = $(window).scrollTop(),
        o = $(".faqPage").offset().top - ($(window).height() - 260);
    if (e <= t)
        if (o < t) {
            var i = o - t;
            $(".planStaticHead").css("position", "fixed"), $(".planStaticHead").css("top", i)
        } else $(".planStaticHead").css("position", "fixed"), $(".planStaticHead").css("width", $(".plan-main-area").width()), $(".planStaticHead").css("top", "0"), $(".planStaticHead").css("z-index", "9");
    else $(".planStaticHead").css("position", ""), $(".planStaticHead").css("width", "100%"), $(".planStaticHead").css("top", "")
}), $(".checkboxUrl").on("click", function() {
    window.location.href = $(this).attr("data-filter")
});