/*------ Image Check Validation -----------*/
function checkSize(input) {
	var fullpath = input.value;
	var str = fullpath;
	var dotIndex = str.lastIndexOf('.');
	var ext = str.substring(dotIndex);
	document.getElementById('error1').innerHTML = '';
	if (ext == ".gif" || ext == ".png" || ext == ".bmp" || ext == ".jpeg" || ext == ".jpg" || ext == ".GIF" ||ext == ".PNG" || ext == ".BMP" || ext == ".JPEG" || ext == ".JPG") {
		if (input.files && input.files[0].size > (3 * 1024 * 1024)) {
			$('#image').val("");
			document.getElementById('error1').innerHTML = "File too large. Max 3MB Allowed.";
		}
	}
	else{
		$('#image').val("");
		document.getElementById('error1').innerHTML = "Photo only allows file types of GIF, PNG, JPG, JPEG and BMP.";				
	}			
	return true;
}
/*------ Resume Check Validation -----------*/
function checkSizeResume(input) {
	var fullpath = input.value;
	var str = fullpath;
	var dotIndex = str.lastIndexOf('.');
	var ext = str.substring(dotIndex);
	document.getElementById('error1').innerHTML = '';
	if (ext == ".pdf" || ext == ".docx" || ext == ".doc" || ext == ".PDF" ||ext == ".DOCX" || ext == ".DOC") {
		if (input.files && input.files[0].size > (3 * 1024 * 1024)) {
			$('#resume').val("");
			document.getElementById('error1').innerHTML = "File too large. Max 3MB Allowed.";
		}
	}
	else{
		$('#resume').val("");
		document.getElementById('error1').innerHTML = "File only allows file types of PDF, DOCX, DOC.";				
	}			
	return true;
}
/*-------------------- Status Change ----------------------------*/
$(function() {
	$(".enable").click(function(event){
		var id = jQuery(this).attr('id'); 
		url = jQuery(this).attr('data-url');
		var data;
		data += '&isAjax=1';
		jQuery('#ajax_loader_wish'+id).show();
		$.ajax({
		type: "GET",
		url : url,
		dataType : 'json',
		data : data,
		success : function(data) {
				//alert(data.message);
				jQuery('#ajax_loader_wish'+id).hide();
				$("#enable"+id).hide();
				$("#disable"+id).show();
			}
		});
	});

	$(".disable").click(function(event){
		var id = jQuery(this).attr('id');
		url = jQuery(this).attr('data-url');
		var data;
		data += '&isAjax=1';
		jQuery('#ajax_loader_wish'+id).show();
		$.ajax({
		type: "GET",
		url : url,
		dataType : 'json',
		data : data,
		success : function(data) {
				//alert(data.message);
				jQuery('#ajax_loader_wish'+id).hide();
				$("#disable"+id).hide();
				$("#enable"+id).show();
			}
		});
	});

});
/*============== Attribute Code validation ==================*/
function validateAlpha(){
    var textInput = document.getElementById("attribute_code").value;
    textInput = textInput.replace(/[^a-z_]/g, "");
    document.getElementById("attribute_code").value = textInput;
}
$('#attribute_code').keypress(function(e) { 
    var s = String.fromCharCode( e.which );
    if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
        alert('caps is on');
    }
});
function validateNum(){
    var textInput = document.getElementById("experience").value;
    textInput = textInput.replace(/[^0-9-]/g, "");
    document.getElementById("experience").value = textInput;
}
/*============== Store Locator Ajax ==================*/
$('#state').change(function(){
	id  = $(this).val();
	url = jQuery('#ctag').attr('data-url');
	//$('#city').find('option:gt(0)').remove();
	$('#city').empty();
	$.get(url, 
	{ option: $(this).val() },
	function(data) {
		$("#city").append(data);
	});
});
/*=============== sitemap js ==========================*/
$('#site-level').change(function(){
	level = $(this).val();
	if(level == 'Level1' || level == ''){
		$('#site-parent_id').attr('disabled', 'true');	
	}else{
		$('#site-parent_id').removeAttr('disabled');
	}
	
});
/*=============== blog multiselect js ==========================*/
$(function () {
	$('#blog_tags').multiselect({
		includeSelectAllOption: true
	});
	/*$('#btnSelected').click(function () {
		var selected = $("#blog_tags option:selected");
		var message = "";
		selected.each(function () {
			message += $(this).text() + " " + $(this).val() + "\n";
		});
		alert(message);
	});*/
});
$(function () {
	$('#publish_date').datetimepicker({
        format: 'YYYY-MM-DD'
    });
	$("#publish_date").on("dp.change", function (e) {
        $('#publish_date').data("DateTimePicker").maxDate(new Date());
    });
});

$(function () {
	$('#expiry_date').datetimepicker({
        format: 'YYYY-MM-DD'
    });
	$("#expiry_date").on("dp.change", function (e) {
        $('#expiry_date').data("DateTimePicker").minDate(new Date());
    });
});
/* students offer */
$(function () {
	$('#offer-country').multiselect({
		includeSelectAllOption: true
	});
});
/* faq section */

$(function () {
	$('#faq-page').multiselect({
		includeSelectAllOption: true
	});
});