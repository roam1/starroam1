<?php

return [
	'status_array' => [
		//''=>'Select One',
		'0'=>'Disabled',
		'1'=>'Enabled'
	],
	'soap_options'	=> [
		'trace'       => 1,     // traces let us look at the actual SOAP messages later
		'exceptions'  => 1 
	],
	'storetype_array' => [
		//''=>'Select One',
		'other'=>'Other Address',
		'regional'=>'Regional Address',
		'corporate'=>'Corporate Address',
		'dealer'=>'Dealer Address',
	],
	'blog_array' => [
		//''=>'Select One',
		'0'=>'No',
		'1'=>'Yes'
	],
	'plantype_array' => [
		//''=>'Select One',
		'Prepaid'=>'Prepaid',
		'Postpaid'=>'Postpaid'
	],
];
